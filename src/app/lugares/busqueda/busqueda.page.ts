import { Component, OnInit } from '@angular/core';
import { Lugar } from '../lugar.model';
import { LugaresService } from '../lugares.service';

@Component({
  selector: 'app-busqueda',
  templateUrl: './busqueda.page.html',
  styleUrls: ['./busqueda.page.scss'],
})
export class BusquedaPage implements OnInit {

  lugaresCargados: Lugar[];

  
  constructor(private lugaresService: LugaresService) { }

  ngOnInit() {
    this.lugaresCargados = this.lugaresService.lugares;
  }

}
