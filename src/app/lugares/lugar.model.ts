export class Lugar{
    constructor(
        public id: number,
        public titulo: string,
        public descripcion: string,
        public imageUrl: string,
        public precio: number,
    ){}
}