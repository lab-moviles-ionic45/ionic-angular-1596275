import { Injectable } from '@angular/core';
import { Lugar } from './lugar.model';

@Injectable({
  providedIn: 'root'
})
export class LugaresService {

  private _lugares: Lugar[] = [
    new Lugar(1, 'Depto. Las Torres', 'excelente ubicacion', 'https://www.inmuebles24.com/noticias/wp-content/uploads/2020/01/departamentos-de-lujo-2-1024x674.jpeg', 1200),
    new Lugar(2, 'Depto. Cancun', 'excelente ubicacion', 'https://img10.naventcdn.com/avisos/18/00/55/67/12/10/1200x1200/119221215.jpg', 1200),
    new Lugar(3, 'Quinta La Esperanza', 'excelente ubicacion', 'https://definicion.de/wp-content/uploads/2013/10/quinta.jpg', 1200),
    new Lugar(4, 'Casa Colorines', 'excelente ubicacion', 'https://www.vivus.es/blog/wp-content/uploads/2019/04/hacerse-una-casa.jpeg', 1200),
  ];

  get lugares(){
    return [...this._lugares];
  }

  constructor() { }
}
