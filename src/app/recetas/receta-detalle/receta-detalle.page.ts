import { RecetasService } from './../recetas.service';
import { Component, OnInit } from '@angular/core';
import { Receta } from '../receta.mode';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-receta-detalle',
  templateUrl: './receta-detalle.page.html',
  styleUrls: ['./receta-detalle.page.scss'],
})
export class RecetaDetallePage implements OnInit {

  receta: Receta;
  
  constructor(private recetaService: RecetasService, 
    private activateRoute: ActivatedRoute,
    private router: Router,
    private alertCtrl: AlertController) { }

  ngOnInit() {
    //console.log('ngOnInit Detalle');
    this.activateRoute.paramMap.subscribe(paramMap => {
      const param: string = 'recetaId';
      if(!paramMap.has(param)){
        this.router.navigate(['/recetas']);
        return;
      }
      const recetaId: number = +paramMap.get(param);
      this.receta = this.recetaService.getReceta(recetaId);
    })
  }

  ngOnDestroy(){
    //console.log('ngOnDestroy Detalle');
  }

  onDeleteReceta(){
    this.alertCtrl.create({
      header: 'Estas seguro ?',
      message: 'Realmente quieres eliminar esta receta ?',
      buttons: [
        {text: 'Cancelar', role: 'cancel'},
        {text: 'Eliminar', handler: () => {
          this.recetaService.deleteReceta(this.receta.id);
          this.router.navigate(['/recetas']);
        }}
      ]
    }).then(alert => {
      alert.present();
    })
  }

}
