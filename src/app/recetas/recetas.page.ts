import { Component, OnInit } from '@angular/core';
import { Receta } from './receta.mode';
import { RecetasService } from './recetas.service';

@Component({
  selector: 'app-recetas',
  templateUrl: './recetas.page.html',
  styleUrls: ['./recetas.page.scss'],
})
export class RecetasPage implements OnInit {

  recetas: Receta[];

  constructor(private recetaService: RecetasService) { 

  }

  ngOnInit() {
    console.log('ngOnInit');
  }

  ionViewWillEnter(){
      this.recetas = this.recetaService.getAllRecetas();
      console.log('ionViewWillEnter');
  }

  ionViewDidEnter(){
    console.log('ionViewDidEnter');
  }

  ionViewWillLeave(){
    console.log('ionViewWillLeave');
  }

  ionViewDidLeave(){
    console.log('ionViewDidLeave');
  }

  ngOnDestroy(){
    console.log('ngOnDestroy');
  }

}
