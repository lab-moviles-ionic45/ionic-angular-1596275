import { Injectable } from '@angular/core';
import { Receta } from './receta.mode';

@Injectable({
  providedIn: 'root'
})
export class RecetasService {

  recetas: Receta[] = [
    {id:1, titulo: 'Pizza', imageUrl: 'https://placeralplato.com/files/2016/01/Pizza-con-pepperoni.jpg', ingredientes: ['pan', 'tomate', 'queso', 'jamon']},
    {id:2, titulo: 'Hamburguesa', imageUrl: 'https://sifu.unileversolutions.com/image/es-MX/recipe-topvisual/2/1260-709/hamburguesa-clasica-50425188.jpg', ingredientes: ['pan', 'captsup', 'tomate', 'queso', 'jamon']},
    {id:3, titulo: 'Tacos', imageUrl: 'https://i.blogs.es/8e3e94/tacos-suadero-la-vicenta-1/1366_2000.jpg', ingredientes: ['tortilla', 'tomate', 'cebolla', 'carne']},
    {id:4, titulo: 'Chilaquiles', imageUrl: 'https://www.superama.com.mx/views/micrositio/recetas/images/masbuscadas/chilaquiles/Web_fotoreceta.jpg', ingredientes: ['tortilla', 'salsa de tomate', 'cebolla', 'cilantro']},
    {id:5, titulo: 'Enchiladas', imageUrl: 'https://cdn2.cocinadelirante.com/sites/default/files/styles/gallerie/public/images/2018/03/receta-de-enchiladas-de-pollo-en-salsa-verde.jpg', ingredientes: ['crema', 'pollo', 'cilantro', 'tomate']}
  ];

  constructor() { }

  getAllRecetas(){
    return [...this.recetas];
  }

  getReceta(recetaId: number){
    return {...this.recetas.find(r => {
      return r.id === recetaId;
    })};
  }

  deleteReceta(recetaId: number){
      this.recetas = this.recetas.filter(receta => {
        return receta.id !== recetaId;
      })
  }
}
